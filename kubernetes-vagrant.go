package main

import (
	"path/filepath"
	"io/ioutil"
	"fmt"
	"os"
	"k8s.io/client-go/1.4/kubernetes"
	"k8s.io/client-go/1.4/tools/clientcmd"
	"k8s.io/client-go/1.4/pkg/api/v1"
	"k8s.io/client-go/1.4/pkg/api"
	kyaml "k8s.io/client-go/1.4/pkg/util/yaml"
	"gopkg.in/yaml.v2"
	"k8s.io/client-go/1.4/pkg/apis/extensions/v1beta1"
	"os/exec"
	"k8s.io/client-go/1.4/pkg/api/unversioned"
	"time"
)

const (
	kubernetesClusterConfigDir = ".build/kubernetes"
	projectsDir = "/vagrant/web"
	kubeConfig = "/root/.kube/config"
	projectsConfig = "/vagrant/kubernetes/projects.yml"
	namespace = "dev"
	PHASE_RUNNING = "Running"
	HYPERKUBE_VERSION = "v1.4.0"
)

type Project struct {
	Name                     string
	Path                     string
	ConfigMaps               map[string]v1.ConfigMap
	Services                 map[string]v1.Service
	Ingresses                map[string]v1beta1.Ingress
	Deployments              map[string]v1beta1.Deployment
	HorizontalPodAutoscalers map[string]v1beta1.HorizontalPodAutoscaler
	ReplicationControllers   map[string]v1.ReplicationController
}
type Resource struct {
	unversioned.TypeMeta `json:",inline"`
	api.ObjectMeta `json:"metadata,omitempty" protobuf:"bytes,1,opt,name=metadata"`
}
type ResourceInterface interface {
	GetNamespace() string
	SetNamespace(string)
	GetName() string
}

func main() {
	config, err := clientcmd.BuildConfigFromFlags("", kubeConfig)
	if err != nil {
		panic(err.Error())
	}
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	ClearClusterState()
	runHyperKube(clientset)
	prepareCluster(clientset)

	projects := getProjectsForImport()
	for _, project := range projects {
		project.Parse()
		project.Import(clientset)
	}
	waitForProjects(clientset)
}
func waitForProjects(clientset *kubernetes.Clientset)  {
	fmt.Println("Waiting for projects")
	for {
		pods, err := clientset.Core().Pods(namespace).List(api.ListOptions{})
		if err != nil {
			panic(err)
		}
		var podRunning []v1.Pod
		for _, pod := range pods.Items {
			if pod.Status.Phase == PHASE_RUNNING {
				podRunning = append(podRunning, pod)
			}
		}
		fmt.Printf("\rRunning pods count: %d / %d", len(podRunning),len(pods.Items))
		if len(pods.Items) > 0 && len(pods.Items) == len(podRunning) {
			break
		}
		time.Sleep(1000 * time.Millisecond)
	}
	fmt.Println("\nDone")
}
func (project *Project) ParseDir(path string) {
	absPath, err := filepath.Abs(project.Path + "/" + path)
	if err != nil {
		panic(err)
	}
	fmt.Println(absPath)
	files, _ := ioutil.ReadDir(absPath)
	for _, f := range files {
		if f.IsDir() == true {
			continue
		}
		yamlFileName, err := filepath.Abs(absPath + "/" + f.Name())
		if err != nil {
			panic(err)
		}
		reader, _ := os.Open(yamlFileName)
		decoder := kyaml.NewYAMLToJSONDecoder(reader)
		resource := Resource{}
		err = decoder.Decode(&resource)
		if err != nil {
			panic(err)
		}
		//bad
		reader, _ = os.Open(yamlFileName)
		decoder = kyaml.NewYAMLToJSONDecoder(reader)
		//bad end
		fmt.Println(resource.Kind)
		switch resource.Kind {
		case "ConfigMap":
			var decodedObj v1.ConfigMap
			decoder.Decode(&decodedObj)
			project.ConfigMaps[decodedObj.Name] = decodedObj
		case "Deployment":
			var decodedObj v1beta1.Deployment
			decoder.Decode(&decodedObj)
			project.Deployments[decodedObj.Name] = decodedObj
		case "Service":
			var decodedObj v1.Service
			decoder.Decode(&decodedObj)
			project.Services[decodedObj.Name] = decodedObj
		case "Ingress":
			var decodedObj v1beta1.Ingress
			decoder.Decode(&decodedObj)
			project.Ingresses[decodedObj.Name] = decodedObj
		case "HorizontalPodAutoscaler":
			var decodedObj v1beta1.HorizontalPodAutoscaler
			decoder.Decode(&decodedObj)
			project.HorizontalPodAutoscalers[decodedObj.Name] = decodedObj
		}
	}
}
func prepareCluster(clientset *kubernetes.Clientset) {
	_, err := clientset.Namespaces().Create(&v1.Namespace{
		TypeMeta: unversioned.TypeMeta{
			Kind:       "Namespace",
			APIVersion: "v1",
		},
		ObjectMeta: v1.ObjectMeta{
			Name: namespace,
		},
	})
	if err != nil {
		panic(err.Error())
	}
	project := NewProject()
	project.Name = "Hyperkube"
	project.Path = "/vagrant/kubernetes/cluster"
	dirs := []string{
		"config",
		"svc",
		"rc",
		"ing",
	}
	for _, path := range dirs {
		project.ParseDir(path)
	}
	project.Import(clientset)
}
func runHyperKube(clientset *kubernetes.Clientset) {
	fmt.Println("Runing hyperkube...")
	var cmd = `docker run -d \
	    --volume=/:/rootfs:ro \
	    --volume=/sys:/sys:rw \
	    --volume=/var/lib/docker/:/var/lib/docker:rw \
	    --volume=/var/lib/kubelet/:/var/lib/kubelet:rw,shared \
	    --volume=/var/run:/var/run:rw \
	    --volume=/root/.docker:/root/.docker \
	    --volume=/vagrant/kubernetes/cluster/hyperkube:/etc/kubernetes \
	    --net=host \
	    --pid=host \
	    --privileged \
	    gcr.io/google_containers/hyperkube-amd64:` + HYPERKUBE_VERSION +  ` \
	    /hyperkube kubelet \
		--containerized \
		--address="0.0.0.0" \
		--hostname-override=127.0.0.1 \
		--api-servers=http://localhost:8080 \
		--config=/etc/kubernetes/manifests \
		--cluster-dns=10.0.0.10 \
		--cluster-domain=cluster.local \
		--resolv-conf=/rootfs/vagrant/kubernetes/cluster/hyperkube/resolv.conf \
		--allow-privileged --v=2`
	cmdN := exec.Command("sh", "-c", cmd)
	cmdN.Stdout = os.Stdout
	cmdN.Stderr = os.Stderr
	err := cmdN.Run()
	if err != nil {
		panic(err)
	}
	fmt.Println("Done")
	fmt.Println("Waiting for cluster...")
	for {
		_, err := clientset.Core().Pods("").List(api.ListOptions{})
		if err == nil {
			break
		}
	}
	fmt.Println("Done")
}
func NewProject() Project {
	var p Project
	p.Deployments = make(map[string]v1beta1.Deployment)
	p.ConfigMaps = make(map[string]v1.ConfigMap)
	p.Services = make(map[string]v1.Service)
	p.HorizontalPodAutoscalers = make(map[string]v1beta1.HorizontalPodAutoscaler)
	p.Ingresses = make(map[string]v1beta1.Ingress)
	p.ReplicationControllers = make(map[string]v1.ReplicationController)
	return p
}
func getProjectsForImport() ([]Project) {
	var (
		projectNames []string
		projects []Project
	)
	yamlFile, err := ioutil.ReadFile(projectsConfig)
	if err != nil {
		panic(err.Error())
	}
	err = yaml.Unmarshal(yamlFile, &projectNames)
	if err != nil {
		panic(err.Error())
	}
	for _, projectName := range projectNames {
		projectKubernetesDir := projectsDir + "/" +
			projectName + "/" + kubernetesClusterConfigDir
		if _, err := os.Stat(projectKubernetesDir); err == nil {
			var project = NewProject()
			project.Name = projectName
			project.Path = projectKubernetesDir

			projects = append(projects, project)
		}
	}

	return projects
}

func (project *Project) Parse() {
	fmt.Println("Parsing project " + project.Name + " configuration")
	dirs := []string{
		"config",
		"deployments",
		"ing",
		"svc",
		"hpa",
	}
	for _, path := range dirs {
		project.ParseDir(path)
		project.ParseDir(path + "/" + namespace)
	}
	fmt.Println("Done")
}

func (project *Project) ImportResource(resource ResourceInterface, clientset *kubernetes.Clientset) {
	if resource.GetNamespace() == "" {
		resource.SetNamespace(namespace)
	}
	var err error
	switch resource.(type) {
	case *v1.ReplicationController:
		convertedObj := resource.(*v1.ReplicationController)
		_, err = clientset.Core().ReplicationControllers(resource.GetNamespace()).Create(convertedObj)
	case *v1.ConfigMap:
		convertedObj := resource.(*v1.ConfigMap)
		_, err = clientset.Core().ConfigMaps(resource.GetNamespace()).Create(convertedObj)
	case *v1beta1.Deployment:
		convertedObj := resource.(*v1beta1.Deployment)
		_, err = clientset.Extensions().Deployments(resource.GetNamespace()).Create(convertedObj)
	case *v1beta1.Ingress:
		convertedObj := resource.(*v1beta1.Ingress)
		_, err = clientset.Extensions().Ingresses(resource.GetNamespace()).Create(convertedObj)
	}
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error: " + resource.GetName() + " " + err.Error())
	} else {
		fmt.Println("Import " + " " + resource.GetName() + " imported")
	}
}

func (project *Project) Import(clientset *kubernetes.Clientset) {
	fmt.Println("Importing project " + project.Name)
	for _, resource := range project.ConfigMaps {
		project.ImportResource(&resource, clientset)
	}
	for _, resource := range project.Services {
		project.ImportResource(&resource, clientset)
	}
	for _, resource := range project.Deployments {
		project.ImportResource(&resource, clientset)
	}
	for _, resource := range project.Ingresses {
		project.ImportResource(&resource, clientset)
	}
	for _, resource := range project.ReplicationControllers {
		project.ImportResource(&resource, clientset)
	}
	fmt.Println("Done")
}
