package main

import (
	"fmt"
	"os/exec"
	"strings"
)

func ClearClusterState() {
	fmt.Println("Clear cluster state...")
	for {
		dockerImagesId, err := exec.Command("docker", "ps", "-q").Output()
		if err != nil {
			panic(err)
		}
		if string(dockerImagesId) == "" {
			break
		}
		dockerImagesIdMap := strings.Split(string(dockerImagesId), "\n")
		for _, id := range dockerImagesIdMap {
			exec.Command("docker", "rm", "-f", id).Output()
		}
	}
	cmd := "umount $(cat /proc/mounts | grep /var/lib/kubelet | awk '{print $2}'|head -n1000)"
	_, err := exec.Command("sh", "-c", cmd).Output()
	if err != nil {
		panic(err)
	}
	cmd = "rm -rf /var/lib/kubelet"
	_, err = exec.Command("sh", "-c", cmd).Output()
	if err != nil {
		panic(err)
	}
	fmt.Println("Done")
}
