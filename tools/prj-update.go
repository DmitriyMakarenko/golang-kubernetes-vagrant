package main

import (
	"os/exec"
	"io/ioutil"
	"fmt"
	"flag"
	"os"
)

const (
	PROJECT_DIR_DEFAULT = "/vagrant/web"

	CMD_HG_DIFF = "hg diff"
	CMD_HG_PULL = "hg pull;hg up -C"
	CMD_COMPOSER_INSTALL = "composer install --ignore-platform-reqs"
)
func main() {
	debug := flag.Bool("v", false, "debug")
	projectDir := flag.String("dir", PROJECT_DIR_DEFAULT, "Project dir")
	forceUpdate := flag.Bool("force", false, "Force update")
	disableComposerInstall := flag.Bool("no-composer-install", false, "Disable composer install execution")
	flag.Parse()

	checkHgExists()
	checkComposerExists()

	projectsList := getDirList(*projectDir)
	if *debug == true {
		fmt.Printf("%+v\n", projectsList)
	}
	for _, projectName := range projectsList {
		projectPath := *projectDir + "/" + projectName
		cmd:= "cd " + projectPath + ";" + CMD_HG_DIFF
		out, err := exec.Command("sh", "-c", cmd).Output()
		if err != nil {
			panic(err)
		}
		fmt.Println(projectName)
		if string(out) == "" || *forceUpdate == true {
			pullProject(projectPath, *debug, *disableComposerInstall)
		} else {
			fmt.Println(string(out))
			fmt.Println("Not empty diff, are you sure? Type 'y' to continue")
			var response string
			_, err = fmt.Scanln(&response)
			if err != nil {
				panic(err)
			}
			if response == "y" {
				pullProject(projectPath, *debug, *disableComposerInstall)
			} else {
				fmt.Println("Skiping...")
			}
		}
	}
}
func getDirList(dir string) []string {
	var projects []string
	files, _ := ioutil.ReadDir(dir)
	for _, f := range files {
		if f.IsDir() == false {
			continue
		}
		if _, err := os.Stat(dir + "/" + f.Name() + "/.hg"); os.IsNotExist(err) {
			continue
		}
		projects = append(projects, f.Name())
	}
	return projects
}
func pullProject(projectPath string, debug bool, disableComposerInstall bool) {
	fmt.Println("Pulling project...")
	cmd:= "cd " + projectPath + ";" + CMD_HG_PULL
	out, err := exec.Command("sh", "-c", cmd).Output()
	if err != nil {
		panic(err)
	}
	if debug == true {
		fmt.Println(string(out))
	}
	fmt.Println("Done")
	if _, err := os.Stat(projectPath + "/composer.json"); !os.IsNotExist(err) && disableComposerInstall == false {
		runComposerInstall(projectPath, debug)
	}
}
func runComposerInstall(projectPath string, debug bool) {
	fmt.Println("Running composer install...")
	cmd:= "cd " + projectPath + ";" + CMD_COMPOSER_INSTALL
	out, err := exec.Command("sh", "-c", cmd).Output()
	if err != nil {
		panic(err)
	}
	if debug == true {
		fmt.Println(string(out))
	}
	fmt.Println("Done")
}
func checkHgExists()  {
	if _, err := exec.Command("hg").Output(); err != nil {
		fmt.Fprintln(os.Stderr, "Hg not installed! \nGot error: ", err)
		os.Exit(1)
	}
}
func checkComposerExists() {
	if _, err := exec.Command("composer").Output(); err != nil {
		fmt.Fprintln(os.Stderr, "Composer not installed! \nGot error: ", err)
		os.Exit(1)
	}
}